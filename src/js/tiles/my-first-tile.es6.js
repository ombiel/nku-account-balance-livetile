var {LiveTile,registerTile,getCredentials} = require("@ombiel/exlib-livetile-tools");
var screenLink = require("@ombiel/aek-lib/screen-link");
var accountCSS = require("../../css/account");
var _ = require("-aek/utils");

class AccoutBalanceTile extends LiveTile {

  onReady() {
    // Debounce render so it doesn't get triggered more than once in a 1 sec period
    this.render = _.throttle(this.render.bind(this),1000);
    this.showOriginalFace(()=>{
        this.setOptions();
        this.fetchData();
      });

  }

  fetchData(){
    this.getTileAttributes();
    var url = screenLink("account-balance-livetile/data");
    // This method is required in order for the live tile to work on native
    return this.ajax({url:url})
    .done((data)=>{
      this.balance = data.DATA.BALANCE;
      this.render();
      this.timer(this.refetchPeriod,this.fetchData.bind(this));
    })
    .fail((err)=>{
      //console.log(err);
    }).always(()=>{
      if(this.fetchTimer) { this.fetchTimer.stop(); }
      this.fetchTimer = this.timer(this.refetchPeriod,this.fetchData.bind(this));
    });
  }

  render(){

    var tileAttributes = this.getTileAttributes();

    if(tileAttributes.image || tileAttributes.img){
      var image = tileAttributes.image || tileAttributes.img;
    }

    var face =
    `<div class=${accountCSS.accountFaceContainer} style="background-image:url(${image})">
        <div class=${accountCSS.balance}>
          <h2>${this.availableBalanceText}${this.currencyPrefix}${this.balance}</h2>
        </div>
      </div>`;

  this.flipFace(face);

}

setOptions(){
  var tileAttributes = this.getTileAttributes();
  var accountOptions = tileAttributes.account || {};
  this.availableBalanceText = accountOptions.availableBalanceText || "Balance: ";
  this.currencyPrefix = accountOptions.currencyPrefix || "$";
  this.refetchPeriod = accountOptions.refreshPeriod || 600000;
}

}

registerTile(AccoutBalanceTile,"accountBalanceTile");
